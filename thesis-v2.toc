\select@language {british}
\contentsline {chapter}{\numberline {1}Introduction}{12}{chapter.1}
\contentsline {chapter}{\numberline {2}Background}{15}{chapter.2}
\contentsline {section}{\numberline {2.1}Quantum fields in curved spacetime}{15}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Canonical quantisation}{16}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Non-uniqueness of the quantum state}{17}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Propagators}{18}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}The discrete SJ state}{19}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Background on causal sets}{20}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Definition}{22}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Alternative definition}{25}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}The continuum SJ state}{26}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Definition}{27}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Explicit construction}{30}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Alternative Definition}{31}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}The Hadamard property}{33}{subsection.2.3.4}
\contentsline {chapter}{\numberline {3}The SJ state in the flat causal diamond in two dimensions}{34}{chapter.3}
\contentsline {section}{\numberline {3.1}The massless scalar field in two-dimensional flat spacetimes}{35}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}The SJ state in Rindler space}{39}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}The massless SJ two-point function in the flat causal diamond}{41}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}The SJ state in the centre and corner}{45}{subsection.3.2.1}
\contentsline {subsubsection}{The centre}{46}{section*.12}
\contentsline {subsubsection}{The corner}{48}{section*.13}
\contentsline {section}{\numberline {3.3}Comparison with the discrete SJ state}{49}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Causal sets and discrete propagators}{50}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}The SJ state in the centre}{50}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}The SJ state in the corner and in the full diamond}{51}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}Conclusions}{54}{section.3.4}
\contentsline {chapter}{\numberline {4}The SJ state in de Sitter space}{57}{chapter.4}
\contentsline {section}{\numberline {4.1}Geometry of de Sitter Space}{58}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}The global patch of de Sitter space, $dS^D$}{60}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}The Poincar\'e patch of de Sitter space, $dS^D_P$}{60}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Vacuum states in de Sitter space}{61}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}BD modes}{62}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Euclidean modes}{62}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Two-point functions and $\alpha $-vacua}{63}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}The SJ state in de Sitter space}{66}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}The SJ state in the Poincar\'e patch}{67}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}The SJ state in the global patch}{70}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}The critical mass $m_*$}{75}{subsection.4.3.3}
\contentsline {section}{\numberline {4.4}The discrete SJ state on a sprinkling into $dS^2$}{76}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Sprinkling into a causal diamond in $dS^2$}{76}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Simulation results}{77}{subsection.4.4.2}
\contentsline {section}{\numberline {4.5}Conclusions}{81}{section.4.5}
\contentsline {chapter}{\numberline {5}Remarks on the SJ state in the trousers spacetime}{84}{chapter.5}
\contentsline {section}{\numberline {5.1}Quantum Fields in the trousers spacetime}{87}{section.5.1}
\contentsline {section}{\numberline {5.2}The pair of diamonds}{89}{section.5.2}
\contentsline {section}{\numberline {5.3}Propagators in the trousers}{91}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Integral form of Green's equation}{92}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}The field equations near the singularity}{94}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}A one-parameter family of propagators}{95}{subsection.5.3.3}
\contentsline {section}{\numberline {5.4}Propagation of plane waves past the singularity}{98}{section.5.4}
\contentsline {section}{\numberline {5.5}Eigenmodes of the Pauli-Jordan function}{99}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Counting eigenmodes}{100}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Ordinary plane waves}{101}{subsection.5.5.2}
\contentsline {subsection}{\numberline {5.5.3}Restricted plane waves}{102}{subsection.5.5.3}
\contentsline {subsection}{\numberline {5.5.4}Ordinary plane waves revisited}{104}{subsection.5.5.4}
\contentsline {subsection}{\numberline {5.5.5}Restricted constant functions}{104}{subsection.5.5.5}
\contentsline {section}{\numberline {5.6}Outlook}{105}{section.5.6}
\contentsline {chapter}{\numberline {6}For Future Investigation}{107}{chapter.6}
\contentsline {chapter}{\numberline {A}Corrections to the SJ two-point function}{109}{appendix.A}
\contentsline {chapter}{\numberline {B}Modes in $dS^D$ and $dS^D_P$}{112}{appendix.B}
\contentsline {section}{\numberline {B.1}Bunch-Davies modes on $dS^D_P$}{112}{section.B.1}
\contentsline {subsubsection}{Klein-Gordon modes:}{112}{section*.34}
\contentsline {subsubsection}{Absolute value of the normalisation constant $n_\mathbf {k}$:}{113}{section*.35}
\contentsline {subsubsection}{Phase of the normalisation constant $n_\mathbf {k}$:}{114}{section*.36}
\contentsline {subsubsection}{Summary:}{114}{section*.37}
\contentsline {section}{\numberline {B.2}Euclidean modes on $dS^D$}{114}{section.B.2}
\contentsline {subsubsection}{Klein-Gordon modes}{115}{section*.38}
\contentsline {subsubsection}{Absolute value of the normalisation constant $n_L$:}{116}{section*.39}
\contentsline {subsubsection}{Phase of the normalisation constant $n_L$:}{118}{section*.40}
\contentsline {subsubsection}{Summary:}{119}{section*.41}
