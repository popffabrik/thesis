\select@language {british}
\contentsline {chapter}{Nomenclature}{10}{chapter*.5}
\contentsline {chapter}{\numberline {1}Introduction}{11}{chapter.1}
\contentsline {chapter}{\numberline {2}Background}{14}{chapter.2}
\contentsline {section}{\numberline {2.1}QFT in curved spacetime}{14}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Canonical Quantisation}{15}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Non-uniqueness of the quantum state}{16}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Propagators}{17}{subsection.2.1.3}
\contentsline {section}{\numberline {2.2}The discrete SJ state}{18}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Background on Causal Sets}{19}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Definition}{21}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Alternative definition}{24}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}The continuum SJ state}{25}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Definition}{25}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Explicit construction}{28}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Alternative Definition}{30}{subsection.2.3.3}
\contentsline {subsection}{\numberline {2.3.4}The Hadamard condition}{31}{subsection.2.3.4}
\contentsline {chapter}{\numberline {3}The SJ state on the flat causal diamond}{32}{chapter.3}
\contentsline {section}{\numberline {3.1}The massless scalar field in two-dimensional flat spacetimes}{33}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}The SJ state in Rindler space}{37}{subsection.3.1.1}
\contentsline {section}{\numberline {3.2}The massless SJ two-point function in the flat causal diamond}{39}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}The SJ state in the centre and corner}{44}{subsection.3.2.1}
\contentsline {subsubsection}{The centre}{44}{section*.11}
\contentsline {subsubsection}{The corner}{46}{section*.12}
\contentsline {section}{\numberline {3.3}Comparison with the discrete SJ vacuum}{47}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Causal sets and discrete propagators}{48}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}The SJ vacuum in the centre}{48}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}The SJ vacuum in the corner and in the full diamond}{49}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}Corrections to the SJ two-point function}{52}{section.3.4}
\contentsline {section}{\numberline {3.5}Conclusions}{55}{section.3.5}
\contentsline {chapter}{\numberline {4}The SJ state in de Sitter space}{57}{chapter.4}
\contentsline {section}{\numberline {4.1}Geometry of de Sitter Space}{57}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}The global patch of de Sitter space $dS^D$}{60}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}The Poincar\'e patch of de Sitter space $dS^D_P$}{60}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Vacuum states in de Sitter space}{61}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}BD modes}{62}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Euclidean modes}{62}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Two-point functions and $\alpha $-vacua}{63}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}The SJ vacuum on de Sitter space}{65}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}The SJ vacuum on the Poincar\'e patch}{66}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}The SJ vacuum on the global patch}{69}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}The discrete SJ state on a sprinkling into $dS^2$}{73}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Sprinkling into a causal diamond in $dS^2$}{73}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Simulation results}{75}{subsection.4.4.2}
\contentsline {section}{\numberline {4.5}Conclusions}{79}{section.4.5}
\contentsline {chapter}{\numberline {5}Remarks on the SJ state in the Trousers Spacetime}{82}{chapter.5}
\contentsline {section}{\numberline {5.1}Quantum Fields in the Trousers}{86}{section.5.1}
\contentsline {section}{\numberline {5.2}The pair of diamonds}{87}{section.5.2}
\contentsline {section}{\numberline {5.3}Propagators in the trousers}{90}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Integral form of Green's equation}{90}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}The field equations near the singularity}{92}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}A one-parameter family of propagators}{93}{subsection.5.3.3}
\contentsline {section}{\numberline {5.4}Propagation of plane waves past the singularity}{96}{section.5.4}
\contentsline {section}{\numberline {5.5}The Pauli-Jordan function and its eigenmodes}{97}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Counting eigenmodes}{97}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Ordinary plane waves}{98}{subsection.5.5.2}
\contentsline {subsection}{\numberline {5.5.3}Restricted plane waves}{99}{subsection.5.5.3}
\contentsline {subsection}{\numberline {5.5.4}Ordinary plane waves revisited}{101}{subsection.5.5.4}
\contentsline {subsection}{\numberline {5.5.5}Restricted constant functions}{101}{subsection.5.5.5}
\contentsline {chapter}{\numberline {A}BD Modes in $dS^D$ and $dS^D_P$}{103}{appendix.A}
\contentsline {section}{\numberline {A.1}Bunch-Davies modes on $dS^D_P$}{103}{section.A.1}
\contentsline {subsubsection}{Klein-Gordon modes:}{103}{section*.33}
\contentsline {subsubsection}{Absolute value of the normalisation constant $n_\mathbf {k}$:}{104}{section*.34}
\contentsline {subsubsection}{Phase of the normalisation constant $n_\mathbf {k}$:}{104}{section*.35}
\contentsline {subsubsection}{Summary:}{105}{section*.36}
\contentsline {section}{\numberline {A.2}Euclidean modes on $dS^D$}{105}{section.A.2}
\contentsline {subsubsection}{Klein-Gordon modes}{106}{section*.37}
\contentsline {subsubsection}{Absolute value of the normalisation constant $n_L$:}{107}{section*.38}
\contentsline {subsubsection}{Phase of the normalisation constant $n_L$:}{109}{section*.39}
\contentsline {subsubsection}{Summary:}{109}{section*.40}
\contentsline {chapter}{\numberline {B}The spacetime version of Stokes' theorem and forward propagation with Green functions}{111}{appendix.B}
