\contentsline {chapter}{\numberline {1}Introduction}{9}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction}{9}{section.1.1}
\contentsline {chapter}{\numberline {2}Background}{12}{chapter.2}
\contentsline {section}{\numberline {2.1}Quantum Field Theory in curved spacetime}{12}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}The standard approach}{12}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Non-uniqueness of the quantum state}{14}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}The SJ vacuum}{15}{section.2.2}
\contentsline {section}{\numberline {2.3}A second definition}{19}{section.2.3}
\contentsline {chapter}{\numberline {3}The SJ state on the flat causal diamond}{24}{chapter.3}
\contentsline {section}{\numberline {3.1}The massless scalar field in two-dimensional flat spacetimes}{24}{section.3.1}
\contentsline {section}{\numberline {3.2}The massless SJ two-point function in the flat causal diamond}{28}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}The SJ state in the centre and corner}{33}{subsection.3.2.1}
\contentsline {subsubsection}{The centre}{34}{section*.8}
\contentsline {subsubsection}{The corner}{35}{section*.9}
\contentsline {section}{\numberline {3.3}Comparison with the discrete SJ vacuum}{37}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Causal sets and discrete propagators}{38}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}The SJ vacuum in the centre}{40}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}The SJ vacuum in the corner and in the full diamond}{42}{subsection.3.3.3}
\contentsline {section}{\numberline {3.4}Corrections to the SJ two-point function}{43}{section.3.4}
\contentsline {section}{\numberline {3.5}Conclusions}{45}{section.3.5}
\contentsline {chapter}{\numberline {4}The SJ state in De Sitter Space}{51}{chapter.4}
\contentsline {section}{\numberline {4.1}Introduction}{51}{section.4.1}
\contentsline {section}{\numberline {4.2}The SJ vacuum on de Sitter space}{54}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}The SJ vacuum on the Poincar\'e patch}{55}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}The SJ vacuum on the global patch}{59}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}The discrete SJ state on a sprinkling into $dS^2$}{62}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Sprinkling into a causal diamond in $dS^2$}{63}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Simulation results}{65}{subsection.4.3.2}
\contentsline {section}{\numberline {4.4}Conclusions}{67}{section.4.4}
\contentsline {section}{\numberline {4.5}Geometry of de Sitter Space}{71}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Global patch of de Sitter ($dS^D$)}{73}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Cosmological/Poincar\'e patch of de Sitter ($dS^D_P$)}{74}{subsection.4.5.2}
\contentsline {section}{\numberline {4.6}Vacuum states on de Sitter space}{74}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Bunch-Davies modes on $dS^D_P$}{75}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}Euclidean modes on $dS^D$}{77}{subsection.4.6.2}
\contentsline {subsection}{\numberline {4.6.3}Two-point functions and $\alpha $-vacua}{81}{subsection.4.6.3}
\contentsline {section}{\numberline {4.7}Calculation of Inner Products}{84}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}Global chart}{84}{subsection.4.7.1}
\contentsline {chapter}{\numberline {5}Some remarks on the SJ state and topology change}{87}{chapter.5}
\contentsline {section}{\numberline {5.1}Quantum Field Theory on the Trousers}{90}{section.5.1}
\contentsline {section}{\numberline {5.2}The pair of diamonds}{92}{section.5.2}
\contentsline {section}{\numberline {5.3}Propagators on the trousers}{95}{section.5.3}
\contentsline {section}{\numberline {5.4}Propagation of plane waves past the singularity}{101}{section.5.4}
\contentsline {section}{\numberline {5.5}The Pauli-Jordan function and its eigenmodes}{102}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Counting eigenmodes}{102}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Ordinary plane waves}{103}{subsection.5.5.2}
\contentsline {subsection}{\numberline {5.5.3}Restricted plane waves}{104}{subsection.5.5.3}
\contentsline {subsection}{\numberline {5.5.4}Ordinary plane waves revisited}{106}{subsection.5.5.4}
\contentsline {subsection}{\numberline {5.5.5}Restricted constant functions}{107}{subsection.5.5.5}
\contentsline {section}{\numberline {.1}Forward propagation using the Green function}{109}{section..1}
