\BOOKMARK [0][-]{chapter.1}{Introduction}{}% 1
\BOOKMARK [0][-]{chapter.2}{Background}{}% 2
\BOOKMARK [1][-]{section.2.1}{Quantum fields in curved spacetime}{chapter.2}% 3
\BOOKMARK [2][-]{subsection.2.1.1}{Canonical quantisation}{section.2.1}% 4
\BOOKMARK [2][-]{subsection.2.1.2}{Non-uniqueness of the quantum state}{section.2.1}% 5
\BOOKMARK [2][-]{subsection.2.1.3}{Propagators}{section.2.1}% 6
\BOOKMARK [1][-]{section.2.2}{The discrete SJ state}{chapter.2}% 7
\BOOKMARK [2][-]{subsection.2.2.1}{Background on causal sets}{section.2.2}% 8
\BOOKMARK [2][-]{subsection.2.2.2}{Definition}{section.2.2}% 9
\BOOKMARK [2][-]{subsection.2.2.3}{Alternative definition}{section.2.2}% 10
\BOOKMARK [1][-]{section.2.3}{The continuum SJ state}{chapter.2}% 11
\BOOKMARK [2][-]{subsection.2.3.1}{Definition}{section.2.3}% 12
\BOOKMARK [2][-]{subsection.2.3.2}{Explicit construction}{section.2.3}% 13
\BOOKMARK [2][-]{subsection.2.3.3}{Alternative Definition}{section.2.3}% 14
\BOOKMARK [2][-]{subsection.2.3.4}{The Hadamard property}{section.2.3}% 15
\BOOKMARK [0][-]{chapter.3}{The SJ state in the flat causal diamond in two dimensions}{}% 16
\BOOKMARK [1][-]{section.3.1}{The massless scalar field in two-dimensional flat spacetimes}{chapter.3}% 17
\BOOKMARK [2][-]{subsection.3.1.1}{The SJ state in Rindler space}{section.3.1}% 18
\BOOKMARK [1][-]{section.3.2}{The massless SJ two-point function in the flat causal diamond}{chapter.3}% 19
\BOOKMARK [2][-]{subsection.3.2.1}{The SJ state in the centre and corner}{section.3.2}% 20
\BOOKMARK [1][-]{section.3.3}{Comparison with the discrete SJ state}{chapter.3}% 21
\BOOKMARK [2][-]{subsection.3.3.1}{Causal sets and discrete propagators}{section.3.3}% 22
\BOOKMARK [2][-]{subsection.3.3.2}{The SJ state in the centre}{section.3.3}% 23
\BOOKMARK [2][-]{subsection.3.3.3}{The SJ state in the corner and in the full diamond}{section.3.3}% 24
\BOOKMARK [1][-]{section.3.4}{Conclusions}{chapter.3}% 25
\BOOKMARK [0][-]{chapter.4}{The SJ state in de Sitter space}{}% 26
\BOOKMARK [1][-]{section.4.1}{Geometry of de Sitter Space}{chapter.4}% 27
\BOOKMARK [2][-]{subsection.4.1.1}{The global patch of de Sitter space, dSD}{section.4.1}% 28
\BOOKMARK [2][-]{subsection.4.1.2}{The Poincar\351 patch of de Sitter space, dSDP}{section.4.1}% 29
\BOOKMARK [1][-]{section.4.2}{Vacuum states in de Sitter space}{chapter.4}% 30
\BOOKMARK [2][-]{subsection.4.2.1}{BD modes}{section.4.2}% 31
\BOOKMARK [2][-]{subsection.4.2.2}{Euclidean modes}{section.4.2}% 32
\BOOKMARK [2][-]{subsection.4.2.3}{Two-point functions and -vacua}{section.4.2}% 33
\BOOKMARK [1][-]{section.4.3}{The SJ state in de Sitter space}{chapter.4}% 34
\BOOKMARK [2][-]{subsection.4.3.1}{The SJ state in the Poincar\351 patch}{section.4.3}% 35
\BOOKMARK [2][-]{subsection.4.3.2}{The SJ state in the global patch}{section.4.3}% 36
\BOOKMARK [2][-]{subsection.4.3.3}{The critical mass m*}{section.4.3}% 37
\BOOKMARK [1][-]{section.4.4}{The discrete SJ state on a sprinkling into dS2}{chapter.4}% 38
\BOOKMARK [2][-]{subsection.4.4.1}{Sprinkling into a causal diamond in dS2}{section.4.4}% 39
\BOOKMARK [2][-]{subsection.4.4.2}{Simulation results}{section.4.4}% 40
\BOOKMARK [1][-]{section.4.5}{Conclusions}{chapter.4}% 41
\BOOKMARK [0][-]{chapter.5}{Remarks on the SJ state in the trousers spacetime}{}% 42
\BOOKMARK [1][-]{section.5.1}{Quantum Fields in the trousers spacetime}{chapter.5}% 43
\BOOKMARK [1][-]{section.5.2}{The pair of diamonds}{chapter.5}% 44
\BOOKMARK [1][-]{section.5.3}{Propagators in the trousers}{chapter.5}% 45
\BOOKMARK [2][-]{subsection.5.3.1}{Integral form of Green's equation}{section.5.3}% 46
\BOOKMARK [2][-]{subsection.5.3.2}{The field equations near the singularity}{section.5.3}% 47
\BOOKMARK [2][-]{subsection.5.3.3}{A one-parameter family of propagators}{section.5.3}% 48
\BOOKMARK [1][-]{section.5.4}{Propagation of plane waves past the singularity}{chapter.5}% 49
\BOOKMARK [1][-]{section.5.5}{Eigenmodes of the Pauli-Jordan function}{chapter.5}% 50
\BOOKMARK [2][-]{subsection.5.5.1}{Counting eigenmodes}{section.5.5}% 51
\BOOKMARK [2][-]{subsection.5.5.2}{Ordinary plane waves}{section.5.5}% 52
\BOOKMARK [2][-]{subsection.5.5.3}{Restricted plane waves}{section.5.5}% 53
\BOOKMARK [2][-]{subsection.5.5.4}{Ordinary plane waves revisited}{section.5.5}% 54
\BOOKMARK [2][-]{subsection.5.5.5}{Restricted constant functions}{section.5.5}% 55
\BOOKMARK [1][-]{section.5.6}{Outlook}{chapter.5}% 56
\BOOKMARK [0][-]{chapter.6}{For Future Investigation}{}% 57
\BOOKMARK [0][-]{appendix.A}{Corrections to the SJ two-point function}{}% 58
\BOOKMARK [0][-]{appendix.B}{Modes in dSD and dSDP}{}% 59
\BOOKMARK [1][-]{section.B.1}{Bunch-Davies modes on dSDP}{appendix.B}% 60
\BOOKMARK [1][-]{section.B.2}{Euclidean modes on dSD}{appendix.B}% 61
